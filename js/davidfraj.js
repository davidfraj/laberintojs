//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
//Obtengo un numero aleatorio entre un numero minimo y maximo
function aleatorioEntre(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
} // FIN de aleatorioEntre(min, max)


//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
//Me creo una funcion para saber que hacer, al hacer click en una pieza del juego
function agregarClickAPieza(){

  //Quito cualquier click anterior
  $('.pieza').unbind( 'click');

  //Al hacer click en la pieza, juego con ella
  $('.pieza').click(function(){

    // Funcion para que me marque desde donde he pinchado, las piezas a las que me puedo mover.
    if(($(this).has('img.'+vectorNombresAvatares[avatarActual]).length>0) && (clickEnAvatar==false)){

      if(puedoMeterPieza==false){
        clickEnAvatar=true;
        idAvatarSeleccionado=$(this).attr('id');
        seleccionarPieza($(this));
      }

    }else if((clickEnAvatar==true)&&($(this).data('activa')==1)){

        //Muevo el avatar
        clickEnAvatar=false;
        //Voy a desmarcar todas:
        $('.pieza').css('opacity','1');
        $('.pieza').data('activa','0');
        //Quito el avatar del sitio actual (TODO: Quito solo el del jugador actual)
        //$('#'+idAvatarSeleccionado).html('');
        $('#'+vectorNombresAvatares[avatarActual]).remove();
        idAvatarSeleccionado='';
        //Pongo el avatar en su sitio
        //vectorAvatares[avatarActual]=$(this).attr('id');
        $(this).append('<img src="images/'+vectorNombresAvatares[avatarActual]+'.png" width="50" class="'+vectorNombresAvatares[avatarActual]+'" id="'+vectorNombresAvatares[avatarActual]+'">');

        //Voy a calcular, si la figura coincide con una de las cartas del jugador actual
        pe=$(this).data('pe');

        //Si el personaje coincide con unas de las cartas del jugador.....
    		if(vectorCartasAvatares[avatarActual].indexOf(pe)!=-1){
    			//alert(pe+' encontrado');
    			//Marco como que he encontrado la carta
    			if($('#carta_'+pe).data('encontrada')==0){
    				//Marco como que he encontrado la carta
    				$('#carta_'+pe).data('encontrada','1');
    				//La difumino un poco
    				$('#carta_'+pe).css('opacity','0.3');
    				//descuento el numero de cartas que faltan para ganar
    				vectorCartasRestantes[avatarActual]--;
    				//Pregunto a ver si alguien ha ganado
    				if(vectorCartasRestantes[avatarActual]==0){
              //Muestro un mensaje
    					alert('El jugador '+(avatarActual+1)+', ha ganado la partida.');
              //Vuelvo al index.html
              location.href ="index.html";
    				}
    			}
    		}else{
    			//alert(pe+' no encontrado');
    		}

        //Actualizo a que avatar le toca jugar
        avatarActual++;
        avatarActual=avatarActual%numeroAvatares;
        $('#jugadores').html('<h3>Turno de '+(avatarActual+1)+' <img src="images/'+vectorNombresAvatares[avatarActual]+'.png" width="50"><span></span></h3>');
        $('#jugadores h3 span').html(' Debes meter una pieza');


        //Una vez movido el jugador, vuelvo a poder meter pieza en el tablero
        puedoMeterPieza=true;

    }else{

      clickEnAvatar=false;
      idAvatarSeleccionado='';
      //Voy a desmarcar todas:
      $('.pieza').css('opacity','1');
      $('.pieza').data('activa','0');

    }


  }); // FIN de Al hacer click en la pieza, juego con ella

} // FIN de agregarClickAPieza()


//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
//Vamos a selecciona la pieza que le digamos
//RECURSIVA!!!!!
function seleccionarPieza(pieza){

  if(pieza.data('activa')!='1'){

    var id=pieza.attr('id');
    var partes=id.split('_');
    var filaPieza=partes[0];
    var columnaPieza=partes[1];

    pieza.css('opacity','0.5'); //la marcamos
    pieza.data('activa','1'); //la establecemos activa

    if(pieza.data('ar')==1){
        if($('#'+(filaPieza-1)+'_'+columnaPieza).data('ab')==1){
          seleccionarPieza($('#'+(filaPieza-1)+'_'+columnaPieza));
        }
    }

    if(pieza.data('ab')==1){
        if($('#'+(parseInt(filaPieza)+1)+'_'+columnaPieza).data('ar')==1){
          seleccionarPieza($('#'+(parseInt(filaPieza)+1)+'_'+columnaPieza));
        }
    }

    if(pieza.data('iz')==1){
        if($('#'+filaPieza+'_'+(columnaPieza-1)).data('de')==1){
          seleccionarPieza($('#'+filaPieza+'_'+(columnaPieza-1)));
        }
    }

    if(pieza.data('de')==1){
        if($('#'+filaPieza+'_'+(parseInt(columnaPieza)+1)).data('iz')==1){
          seleccionarPieza($('#'+filaPieza+'_'+(parseInt(columnaPieza)+1)));
        }
    }

  }

} // FIN de seleccionarPieza($pieza)


//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
//Funcion para agregar la pieza disponible
function dibujaPiezaDisponible(){

  //Vacio mis piezas, y agrego la ultima
  $('#mispiezas').html('');

  //Dibujo las piezas
  //La primera pieza es sin rotacion, asi que la dibujo tal cual
  switch(piezaDisponibleActualTi){
    case 1:
      piezaDisponibleActualAr=0;
      piezaDisponibleActualDe=1;
      piezaDisponibleActualAb=1;
      piezaDisponibleActualIz=0;
      break;
    case 2:
      piezaDisponibleActualAr=1;
      piezaDisponibleActualDe=0;
      piezaDisponibleActualAb=1;
      piezaDisponibleActualIz=0;
      break;
    case 3:
      piezaDisponibleActualAr=1;
      piezaDisponibleActualDe=1;
      piezaDisponibleActualAb=1;
      piezaDisponibleActualIz=0;
      break;
  }
  $('#mispiezas').append('<div class="mipieza" id="" data-ar="'+piezaDisponibleActualAr+'" data-de="'+piezaDisponibleActualDe+'" data-ab="'+piezaDisponibleActualAb+'" data-iz="'+piezaDisponibleActualIz+'" data-pe="'+piezaDisponibleActualPe+'" data-tipo="'+piezaDisponibleActualTi+'" data-imagen="'+piezaDisponibleActual+'" data-rotacion="0" style="background-image:url(images/'+piezaDisponibleActual+'.png); background-size: 80px 80px; transform: rotate(0deg);"></div>');

  //La segunda pieza, rota 90 grados, asi que calculo su rotacion
  switch(piezaDisponibleActualTi){
    case 1:
      piezaDisponibleActualAr=0;
      piezaDisponibleActualDe=0;
      piezaDisponibleActualAb=1;
      piezaDisponibleActualIz=1;
      break;
    case 2:
      piezaDisponibleActualAr=0;
      piezaDisponibleActualDe=1;
      piezaDisponibleActualAb=0;
      piezaDisponibleActualIz=1;
      break;
    case 3:
      piezaDisponibleActualAr=0;
      piezaDisponibleActualDe=1;
      piezaDisponibleActualAb=1;
      piezaDisponibleActualIz=1;
      break;
  }
  $('#mispiezas').append('<div class="mipieza" id="" data-ar="'+piezaDisponibleActualAr+'" data-de="'+piezaDisponibleActualDe+'" data-ab="'+piezaDisponibleActualAb+'" data-iz="'+piezaDisponibleActualIz+'" data-pe="'+piezaDisponibleActualPe+'" data-tipo="'+piezaDisponibleActualTi+'" data-imagen="'+piezaDisponibleActual+'" data-rotacion="90" style="background-image:url(images/'+piezaDisponibleActual+'.png); background-size: 80px 80px; transform: rotate(90deg);"></div>');

  //La tercera pieza, rota 180 grados, asi que calculo su rotacion
  switch(piezaDisponibleActualTi){
    case 1:
      piezaDisponibleActualAr=1;
      piezaDisponibleActualDe=0;
      piezaDisponibleActualAb=0;
      piezaDisponibleActualIz=1;
      break;
    case 2:
      piezaDisponibleActualAr=1;
      piezaDisponibleActualDe=0;
      piezaDisponibleActualAb=1;
      piezaDisponibleActualIz=0;
      break;
    case 3:
      piezaDisponibleActualAr=1;
      piezaDisponibleActualDe=0;
      piezaDisponibleActualAb=1;
      piezaDisponibleActualIz=1;
      break;
  }
  $('#mispiezas').append('<div class="mipieza" id="" data-ar="'+piezaDisponibleActualAr+'" data-de="'+piezaDisponibleActualDe+'" data-ab="'+piezaDisponibleActualAb+'" data-iz="'+piezaDisponibleActualIz+'" data-pe="'+piezaDisponibleActualPe+'" data-tipo="'+piezaDisponibleActualTi+'" data-imagen="'+piezaDisponibleActual+'" data-rotacion="180" style="background-image:url(images/'+piezaDisponibleActual+'.png); background-size: 80px 80px; transform: rotate(180deg);"></div>');

  //La cuarta pieza, rota 270 grados, asi que calculo su rotacion
  switch(piezaDisponibleActualTi){
    case 1:
      piezaDisponibleActualAr=1;
      piezaDisponibleActualDe=1;
      piezaDisponibleActualAb=0;
      piezaDisponibleActualIz=0;
      break;
    case 2:
      piezaDisponibleActualAr=0;
      piezaDisponibleActualDe=1;
      piezaDisponibleActualAb=0;
      piezaDisponibleActualIz=1;
      break;
    case 3:
      piezaDisponibleActualAr=1;
      piezaDisponibleActualDe=1;
      piezaDisponibleActualAb=0;
      piezaDisponibleActualIz=1;
      break;
  }
  $('#mispiezas').append('<div class="mipieza" id="" data-ar="'+piezaDisponibleActualAr+'" data-de="'+piezaDisponibleActualDe+'" data-ab="'+piezaDisponibleActualAb+'" data-iz="'+piezaDisponibleActualIz+'" data-pe="'+piezaDisponibleActualPe+'" data-tipo="'+piezaDisponibleActualTi+'" data-imagen="'+piezaDisponibleActual+'" data-rotacion="270" style="background-image:url(images/'+piezaDisponibleActual+'.png); background-size: 80px 80px; transform: rotate(270deg);"></div>');

  //Reestablezco todo, como si estuviera a 0 grados de rotacion
  switch(piezaDisponibleActualTi){
    case 1:
      piezaDisponibleActualAr=0;
      piezaDisponibleActualDe=1;
      piezaDisponibleActualAb=1;
      piezaDisponibleActualIz=0;
      break;
    case 2:
      piezaDisponibleActualAr=1;
      piezaDisponibleActualDe=0;
      piezaDisponibleActualAb=1;
      piezaDisponibleActualIz=0;
      break;
    case 3:
      piezaDisponibleActualAr=1;
      piezaDisponibleActualDe=1;
      piezaDisponibleActualAb=1;
      piezaDisponibleActualIz=0;
      break;
  }

  //Añado la funcionalidad
  //Al hacer click en mipieza, la selecciono
  $('.mipieza').click(function(){
    $('.mipieza').css('border', '0px solid black');
    $(this).css('border', '3px solid black');
    piezaRotacion=$(this).data('rotacion');
    piezaImagen=$(this).data('imagen');
    piezaDisponibleActualAr=$(this).data('ar');
    piezaDisponibleActualDe=$(this).data('de');
    piezaDisponibleActualAb=$(this).data('ab');
    piezaDisponibleActualIz=$(this).data('iz');
    piezaDisponibleActualPe=$(this).data('pe');
    piezaDisponibleActualTi=$(this).data('tipo');
  });

  //Agrego click a la pieza
  agregarClickAPieza();

} // FIN de function dibujaPiezaDisponible()


//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
//Funcion para empezar un nuevo juego
function comenzarPartida(){

  //Iniciamos el tablero con los avatares
  for(i=0;i<numeroAvatares;i++){
    $('#'+vectorAvatares[i]).append('<img src="images/'+vectorNombresAvatares[i]+'.png" width="50" class="'+vectorNombresAvatares[i]+'" id="'+vectorNombresAvatares[i]+'">');
  }

  //Dibujamos a quien le toca
  $('#jugadores').html('<h3>Turno de '+(avatarActual+1)+' <img src="images/'+vectorNombresAvatares[avatarActual]+'.png" width="50"><span></span></h3>');
  $('#jugadores h3 span').html(' Debes meter una pieza');

  //Dibujamos las cartas de los jugadores
  for(i=0;i<numeroAvatares;i++){
    $('#cartasjugadores').append('<header>Cartas del jugador '+(i+1)+' <img src="images/'+vectorNombresAvatares[i]+'.png" width="50">:</header><br>');
    $('#cartasjugadores').append('<article>');
    for(j=0;j<vectorCartasAvatares[i].length;j++){
      $('#cartasjugadores').append('<img src="images/cartas/carta'+vectorCartasAvatares[i][j]+'.png" width="50" data-imagen="'+vectorCartasAvatares[i][j]+'" id="carta_'+vectorCartasAvatares[i][j]+'" class="carta" data-encontrada="0">');
    }
    $('#cartasjugadores').append('</article>');
    if(i<(numeroAvatares-1)){
      $('#cartasjugadores').append('<hr>');
    }
  }


} // FIN de function comenzarPartida()


//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
//Cuando se cargue el documento.... que hago
$(document).ready(function(){

  //A ver si funciona el traer datos por GET
  Requests = {
    QueryString : function(item){
      var svalue = location.search.match(new RegExp("[\?\&]" + item + "=([^\&]*)(\&?)","i"));
      return svalue ? svalue[1] : svalue;
    }
  }
  //Constantes a CONFIGURAR para el JUEGO
  numeroAvatares=Requests.QueryString("jugadores"); //ESTO ES EL NUMERO DE JUGADORES
  duracion=Requests.QueryString("duracion"); //ESTO ES LA DURACION DE LA PARTIDA

  //Numero de cartas por jugador
  numeroCartas1=24;
  numeroCartas2=12;
  numeroCartas3=8;
  numeroCartas4=6;
  //Segun la duracion, cambio el numero de cartas
  switch(duracion){
    case '1':
      //La duracion sera un tercio de la partida
      numeroCartas1=numeroCartas1/3;
      numeroCartas2=numeroCartas2/3;
      numeroCartas3=numeroCartas3/3;
      numeroCartas4=numeroCartas4/3;
      break;
    case '2':
      //La duracion sera de la mitad de la partida
      numeroCartas1=numeroCartas1/2;
      numeroCartas2=numeroCartas2/2;
      numeroCartas3=numeroCartas3/2;
      numeroCartas4=numeroCartas4/2;
      break;
    case '3':
      //La duracion sera de todas las cartas
      numeroCartas1=numeroCartas1/1;
      numeroCartas2=numeroCartas2/1;
      numeroCartas3=numeroCartas3/1;
      numeroCartas4=numeroCartas4/1;
      break;
  }
  

  //Para el juego, primero hare click en el avatar, y acto seguido, en el sitio de destino. Solo si he pulsado antes en el avatar, se movera
  clickEnAvatar=false;
  avatarActual=0;
  puedoMeterPieza=true; //Para saber si puedo o no meter una pieza
  vectorAvatares=['7_1','7_7','1_1','1_7'];
  vectorNombresAvatares=['avatar01', 'avatar02', 'avatar03', 'avatar04'];
  idAvatarSeleccionado='';

  //Vectores de cartas para los jugadores al iniciar la partida TODO: Cartas ALEATORIAS
  //Lo unico que queda, es añadir las cartas con sus numeros a la carpeta images/cartas
  vectorCartasAvataresInicial=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24];
  vectorCartasInicialAleatorio=vectorCartasAvataresInicial.sort(function(a, b){return 0.5 - Math.random()});

  //y generar los aleatorios
  vectorCartasAvatares=[[],[],[],[]];
  //dependiendo del numero de jugadores, pongo unas cartas u otro
  switch(numeroAvatares){
    case '1':
      for(i=0;i<numeroCartas1;i++){
        vectorCartasAvatares[0][i]=vectorCartasInicialAleatorio[i];
      }
      vectorCartasRestantes=[numeroCartas1];
      break;
    case '2':
      for(i=0;i<numeroCartas2;i++){
        vectorCartasAvatares[0][i]=vectorCartasInicialAleatorio[i];
        vectorCartasAvatares[1][i]=vectorCartasInicialAleatorio[i+numeroCartas2];
      }
      vectorCartasRestantes=[numeroCartas2,numeroCartas2];
      break;
    case '3':
      for(i=0;i<numeroCartas3;i++){
        vectorCartasAvatares[0][i]=vectorCartasInicialAleatorio[i];
        vectorCartasAvatares[1][i]=vectorCartasInicialAleatorio[i+numeroCartas3];
        vectorCartasAvatares[2][i]=vectorCartasInicialAleatorio[i+(numeroCartas3*2)];
      }
      vectorCartasRestantes=[numeroCartas3,numeroCartas3,numeroCartas3];
      break;
    case '4':
      for(i=0;i<numeroCartas4;i++){
        vectorCartasAvatares[0][i]=vectorCartasInicialAleatorio[i];
        vectorCartasAvatares[1][i]=vectorCartasInicialAleatorio[i+numeroCartas4];
        vectorCartasAvatares[2][i]=vectorCartasInicialAleatorio[i+(numeroCartas4*2)];
        vectorCartasAvatares[3][i]=vectorCartasInicialAleatorio[i+(numeroCartas4*3)];
      }
      vectorCartasRestantes=[numeroCartas4,numeroCartas4,numeroCartas4,numeroCartas4];
      break;
  }


  //Me creo un vector con las piezas que NO son aleatorias
  vectorPiezasFijas=[1,3,5,7,15,17,19,21,29,31,33,35,43,45,47,49];

  //Me creo un vector de piezas disponibles para mi tablero, para no repetir piezas
  vectorPiezasDisponibles=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34];
  vectorPiezasDisponiblesInicio=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34];

  //Me creo varios vectores, con informacion de cada una de las piezas, para guardar sus paredes, y su muñeco
  vectorPiezasDisponiblesAr=[0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,0,1,0,0,1,0];
  vectorPiezasDisponiblesDe=[0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1];
  vectorPiezasDisponiblesAb=[0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
  vectorPiezasDisponiblesIz=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
  vectorPiezasDisponiblesPe=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12];

  //Me creo un vector para los tipos de piezas, siendo 0(ninguna), 1(Para la L), 2(Para la |), 3(para la T)
  vectorPiezasDisponiblesTi=[0,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,3,1,3,1,3,3,1,3,1,1,3,1];

  //Me creo un vector de Personajes, asociados con nombre
  vectorNombresPersonajes=['ninguno', 'dragon', 'buho', 'enano', 'lagarto', 'demoniofaro', 'fantasma', 'cucaracha', 'hada', 'polilla', 'rata', 'murcielago', 'araña'];
  //Me creo un vector de Personajes, asociados con nombre 2
  vectorNombresPersonajesNumeros=['ninguno', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  //Me creo un vector de piezas que han salido
  vectorPiezasEnJuego=[];

  //Guardo la pieza disponible para jugar (SOLO HAY UNA EN TODO MOMENTO)
  piezaDisponibleActual=0;
  piezaDisponibleActualAr=0;
  piezaDisponibleActualDe=0;
  piezaDisponibleActualAb=0;
  piezaDisponibleActualIz=0;
  piezaDisponibleActualPe=0;
  piezaDisponibleActualTi=0;

  //Me creo una variable global, para el numero de pieza y la rotacion de la pieza seleccionada
  piezaRotacion=0;
  piezaImagen=1;

  //Me creo un bucle para generar las piezas ALEATORIAS en el tablero
  contador=1;
  contadorPieza=1;

  for(fila=1; fila<=7;fila++){
    for(columna=1; columna<=7;columna++){

      //Si el contador NO esta en el array de vectorPiezasFijas, pues pongo la pieza
      if(vectorPiezasFijas.indexOf(contador)==-1){
        //Me creo un aleatorio para obtener una posicion de ficha
        numAleatorio=aleatorioEntre(0,vectorPiezasDisponibles.length-contadorPieza);

        vectorPiezasEnJuego[contadorPieza-1]=vectorPiezasDisponibles[numAleatorio];

        vectorPiezasDisponibles[numAleatorio]=vectorPiezasDisponibles[vectorPiezasDisponibles.length-contadorPieza];

        //Me creo un aleatoro para su rotacion
        rotaciones=[0, 90, 180, 270];
        rotacion=rotaciones[aleatorioEntre(0,3)];

        //Calculo las paredes abiertas y cerradas de la pieza (Segun rotacion 0)
        paredAr=vectorPiezasDisponiblesAr[vectorPiezasEnJuego[contadorPieza-1]];
        paredDe=vectorPiezasDisponiblesDe[vectorPiezasEnJuego[contadorPieza-1]];
        paredAb=vectorPiezasDisponiblesAb[vectorPiezasEnJuego[contadorPieza-1]];
        paredIz=vectorPiezasDisponiblesIz[vectorPiezasEnJuego[contadorPieza-1]];
        piezaTipo=vectorPiezasDisponiblesTi[vectorPiezasEnJuego[contadorPieza-1]];

        //Cambio las paredes abiertas o cerradas conforme a su rotacion
        switch(piezaTipo){
          case 1:
            switch(rotacion){
              case 90:
                paredAr=0;
                paredDe=0;
                paredAb=1;
                paredIz=1;
                break;
              case 180:
                paredAr=1;
                paredDe=0;
                paredAb=0;
                paredIz=1;
                break;
              case 270:
                paredAr=1;
                paredDe=1;
                paredAb=0;
                paredIz=0;
                break;
            }
            break;
          case 2:
            switch(rotacion){
              case 90:
              case 270:
                paredAr=0;
                paredDe=1;
                paredAb=0;
                paredIz=1;
                break;
            }
            break;
          case 3:
            switch(rotacion){
              case 90:
                paredAr=0;
                paredDe=1;
                paredAb=1;
                paredIz=1;
                break;
              case 180:
                paredAr=1;
                paredDe=0;
                paredAb=1;
                paredIz=1;
                break;
              case 270:
                paredAr=1;
                paredDe=1;
                paredAb=0;
                paredIz=1;
                break;
            }
            break;
        }

        //Establezco el personaje de la pieza
        personaje=vectorPiezasDisponiblesPe[vectorPiezasEnJuego[contadorPieza-1]];

        //Creo la pieza
        piezadiv='<div data-ar="'+paredAr+'" data-de="'+paredDe+'" data-ab="'+paredAb+'" data-iz="'+paredIz+'" data-pe="'+personaje+'" data-tipo="'+piezaTipo+'" data-imagen="'+vectorPiezasEnJuego[contadorPieza-1]+'" data-rotacion="'+rotacion+'" class="pieza" id="'+fila+'_'+columna+'" style="top: '+(fila*100)+'px; left: '+(columna*100)+'px; background-image:url(images/'+vectorPiezasEnJuego[contadorPieza-1]+'.png); background-size: 100px 100px; transform: rotate('+rotacion+'deg);"></div>';

        //agrego la pieza
        $('#tablero').append(piezadiv);

        //incremento contadorPieza
        contadorPieza++;
      }
      //FIN de Si el contador NO esta en el array de vectorPiezasFijas, pues pongo la pieza

      //Incremento contador
      contador++;

    } // FIN for(columna=1;
  } // FIN for(fila=1;
  
  //Guardo la ultima pieza
  piezaDisponibleActual=vectorPiezasDisponibles[0];
  piezaDisponibleActualAr=vectorPiezasDisponiblesAr[vectorPiezasDisponibles[0]];
  piezaDisponibleActualDe=vectorPiezasDisponiblesDe[vectorPiezasDisponibles[0]];
  piezaDisponibleActualAb=vectorPiezasDisponiblesAb[vectorPiezasDisponibles[0]];
  piezaDisponibleActualIz=vectorPiezasDisponiblesIz[vectorPiezasDisponibles[0]];
  piezaDisponibleActualPe=vectorPiezasDisponiblesPe[vectorPiezasDisponibles[0]];
  piezaDisponibleActualTi=vectorPiezasDisponiblesTi[vectorPiezasDisponibles[0]];
  //dibujo la pieza disponible
  dibujaPiezaDisponible();

  //Al hacer click en el boton, agrego una nueva pieza
  $('.boton').click(function(){

  if(puedoMeterPieza==true){

    puedoMeterPieza=false;
    $('#jugadores h3 span').html(' Debes mover tu avatar');

    //Establezco la imagen que pondre en la pieza
    contador=piezaImagen;
    rotacion=piezaRotacion;

    var id=$(this).attr('id');
    var partes=id.split('_');
    switch(partes[0]){
      case 'fila':
          switch(partes[2]){
            case 'izquierda':
                //agrego la pieza al principio de la fila
                $('#tablero').append('<div data-ar="'+piezaDisponibleActualAr+'" data-de="'+piezaDisponibleActualDe+'" data-ab="'+piezaDisponibleActualAb+'" data-iz="'+piezaDisponibleActualIz+'" data-pe="'+piezaDisponibleActualPe+'" data-tipo="'+piezaDisponibleActualTi+'" data-imagen="'+piezaDisponibleActual+'" data-rotacion="'+rotacion+'" class="pieza" id="'+partes[1]+'_0" style="top:'+partes[1]+'00px; left: 0px; background-image:url(images/'+piezaDisponibleActual+'.png); background-size: 100px 100px; transform: rotate('+rotacion+'deg);"></div>');
                //Muevo todas las piezas una posicion hacia la derecha
                for(i=0;i<=7;i++){
                  $('#'+partes[1]+'_'+i).animate({'left':'+=100px'});
                }
                //Me quedo con la imagen y tipo que he quitado y personaje
                piezaDisponibleActual=$('#'+partes[1]+'_7').data('imagen');
                piezaDisponibleActualTi=$('#'+partes[1]+'_7').data('tipo');
                piezaDisponibleActualPe=$('#'+partes[1]+'_7').data('pe');
                //Compruebo si tiene un personaje, y lo pongo en el otro extremo
                for(i=0;i<numeroAvatares;i++){
                  if(($('#'+partes[1]+'_7').has('img.'+vectorNombresAvatares[i]).length>0)){
                    $('#'+partes[1]+'_0').append('<img src="images/'+vectorNombresAvatares[i]+'.png" width="50" class="'+vectorNombresAvatares[i]+'" id="'+vectorNombresAvatares[i]+'">');
                  }
                }
                //Quito la pieza de mas a la derecha (TODO: Que se haga despues de la animacion)
                $('#'+partes[1]+'_7').remove();
                //Renombro todas las piezas (su ID)(en orden inverso)
                for(i=6;i>=0;i--){
                  $('#'+partes[1]+'_'+i).attr('id',partes[1]+'_'+(i+1));
                }
              break;
            case 'derecha':
                //agrego la pieza al final de la fila
                $('#tablero').append('<div data-ar="'+piezaDisponibleActualAr+'" data-de="'+piezaDisponibleActualDe+'" data-ab="'+piezaDisponibleActualAb+'" data-iz="'+piezaDisponibleActualIz+'" data-pe="'+piezaDisponibleActualPe+'" data-tipo="'+piezaDisponibleActualTi+'" data-imagen="'+piezaDisponibleActual+'" data-rotacion="'+rotacion+'" class="pieza" id="'+partes[1]+'_8" style="top:'+partes[1]+'00px; left: 800px; background-image:url(images/'+piezaDisponibleActual+'.png); background-size: 100px 100px; transform: rotate('+rotacion+'deg);"></div>');
                //Muevo todas las piezas una posicion hacia la izquierda
                for(i=1;i<=8;i++){
                  $('#'+partes[1]+'_'+i).animate({'left':'-=100px'});
                }
                //Me quedo con la imagen y tipo que he quitado y personaje
                piezaDisponibleActual=$('#'+partes[1]+'_1').data('imagen');
                piezaDisponibleActualTi=$('#'+partes[1]+'_1').data('tipo');
                piezaDisponibleActualPe=$('#'+partes[1]+'_1').data('pe');
                //Compruebo si tiene un personaje, y lo pongo en el otro extremo
                for(i=0;i<numeroAvatares;i++){
                  if(($('#'+partes[1]+'_1').has('img.'+vectorNombresAvatares[i]).length>0)){
                    $('#'+partes[1]+'_8').append('<img src="images/'+vectorNombresAvatares[i]+'.png" width="50" class="'+vectorNombresAvatares[i]+'" id="'+vectorNombresAvatares[i]+'">');
                  }
                }
                //Quito la pieza de mas a la izquierda (TODO: Que se haga despues de la animacion)
                $('#'+partes[1]+'_1').remove();
                //Renombro todas las piezas (su ID)(en orden inverso)
                for(i=2;i<=8;i++){
                  $('#'+partes[1]+'_'+i).attr('id',partes[1]+'_'+(i-1));
                }
              break;
          }
        break;
      case 'columna':
          switch(partes[2]){
            case 'arriba':
                //agrego la pieza al principio de la columna
                $('#tablero').append('<div data-ar="'+piezaDisponibleActualAr+'" data-de="'+piezaDisponibleActualDe+'" data-ab="'+piezaDisponibleActualAb+'" data-iz="'+piezaDisponibleActualIz+'" data-pe="'+piezaDisponibleActualPe+'" data-tipo="'+piezaDisponibleActualTi+'" data-imagen="'+piezaDisponibleActual+'" data-rotacion="'+rotacion+'" class="pieza" id="0_'+partes[1]+'" style="top: 0px; left: '+partes[1]+'00px; background-image:url(images/'+piezaDisponibleActual+'.png); background-size: 100px 100px; transform: rotate('+rotacion+'deg);"></div>');
                //Muevo todas hacia abajo
                for(i=0;i<=7;i++){
                  $('#'+i+'_'+partes[1]).animate({'top':'+=100px'});
                }
                //Me quedo con la imagen y tipo que he quitado y personaje
                piezaDisponibleActual=$('#7_'+partes[1]).data('imagen');
                piezaDisponibleActualTi=$('#7_'+partes[1]).data('tipo');
                piezaDisponibleActualPe=$('#7_'+partes[1]).data('pe');
                //Compruebo si tiene un personaje, y lo pongo en el otro extremo
                for(i=0;i<numeroAvatares;i++){
                  if(($('#7_'+partes[1]).has('img.'+vectorNombresAvatares[i]).length>0)){
                    $('#0_'+partes[1]).append('<img src="images/'+vectorNombresAvatares[i]+'.png" width="50" class="'+vectorNombresAvatares[i]+'" id="'+vectorNombresAvatares[i]+'">');
                  }
                }
                //Quito la pieza de mas a abajo (TODO: Que se haga despues de la animacion)
                $('#7_'+partes[1]).remove();
                //Renombro todas las piezas (su ID)(en orden inverso)
                for(i=6;i>=0;i--){
                  $('#'+i+'_'+partes[1]).attr('id',(i+1)+'_'+partes[1]);
                }
              break;
            case 'abajo':
                //agrego la pieza al final de la columna
                $('#tablero').append('<div data-ar="'+piezaDisponibleActualAr+'" data-de="'+piezaDisponibleActualDe+'" data-ab="'+piezaDisponibleActualAb+'" data-iz="'+piezaDisponibleActualIz+'" data-pe="'+piezaDisponibleActualPe+'" data-tipo="'+piezaDisponibleActualTi+'" data-imagen="'+piezaDisponibleActual+'" data-rotacion="'+rotacion+'" class="pieza" id="8_'+partes[1]+'" style="top: 800px; left: '+partes[1]+'00px; background-image:url(images/'+piezaDisponibleActual+'.png); background-size: 100px 100px; transform: rotate('+rotacion+'deg);"></div>');
                //muevo todas hacia arriba
                for(i=1;i<=8;i++){
                  $('#'+i+'_'+partes[1]).animate({'top':'-=100px'});
                }
                //Me quedo con la imagen y tipo que he quitado y personaje
                piezaDisponibleActual=$('#1_'+partes[1]).data('imagen');
                piezaDisponibleActualTi=$('#1_'+partes[1]).data('tipo');
                piezaDisponibleActualPe=$('#1_'+partes[1]).data('pe');
                //Compruebo si tiene un personaje, y lo pongo en el otro extremo
                for(i=0;i<numeroAvatares;i++){
                  if(($('#1_'+partes[1]).has('img.'+vectorNombresAvatares[i]).length>0)){
                    $('#8_'+partes[1]).append('<img src="images/'+vectorNombresAvatares[i]+'.png" width="50" class="'+vectorNombresAvatares[i]+'" id="'+vectorNombresAvatares[i]+'">');
                  }
                }
                //Quito la pieza de mas arriba (TODO: Que se haga despues de la animacion)
                $('#1_'+partes[1]).remove();
                //Renombro todas las piezas (su ID)(en orden inverso)
                for(i=2;i<=8;i++){
                  $('#'+i+'_'+partes[1]).attr('id',(i-1)+'_'+partes[1]);
                }
              break;
          }  
        break;
    } //Fin del switch de fila/columna

    //Dibujo la pieza disponible
    dibujaPiezaDisponible();

  } // Fin de si puedo if(puedoMeterPieza==true)

  }); // FIN de Al hacer click en el boton, agrego una nueva pieza

  //Agrego click a la pieza
  agregarClickAPieza();

  //Llamo a la funcion para comenzar la partida
  comenzarPartida();

}); //Fin de $(document).ready();

